#!/bin/bash

apt-get update
apt-get -y upgrade

# Install required packages
apt-get -y install \
    texlive \
    texinfo \
    pdf2svg \
    librsvg2-bin \
    pandoc \
    pandoc-citeproc \
    python3-pandocfilters \
    m4 \
    html-xml-utils \
    zip \
    unzip \
    gpg \
    wget \
    curl \
    bc \
    inkscape \
    apt-utils


# Install texlive packages
tlmgr init-usertree
tlmgr install stmaryrd enumitem cleveref braket xypic
updmap-sys

# Install nodejs and its packages
curl -fsSL https://deb.nodesource.com/setup_lts.x > nodeinst
bash ./nodeinst
rm ./nodeinst
apt-get install -y nodejs
npm init --yes
npm install -g html-minifier
npm install -g svgo
npm install -g stylelint
npm install -g stylelint-config-standard
npm install -g cssnano
npm install -g postcss postcss-cli

# Installing calibre from the repository also installs a lot of
# unwanted dependencies. We only need the ebook-convert
mkdir calibre && cd calibre
curl "https://github.com/kovidgoyal/calibre/releases/latest" > rawlink
cut -d'"' -f2 rawlink | sed -E "s|$|/calibre-"$(cut -d"/" -f8 rawlink | cut -d'"' -f1 | tr -d "v")"-x86_64.txz|" | sed -E "s|tag|download|g" > link
xargs -n1 curl -OL < link
tar xvJf $(ls -1 | grep -E ".*-x86_64\.txz")
rm link rawlink $(ls -1 | grep -E ".*-x86_64\.txz")
cd .. && mv calibre /opt

# Clean the retrieved package files
apt-get autoremove
apt-get clean
sync
